<img src="./s/bi.png" align="center" width="200">

----

<h1 align="center"><code> 📔SADAQAWARE-LICENSE📔 <code></h1>
<h2 align="center"><i> License development work </i></h2>

----
1. [RPD](#rpd)
2. [Motivation](#motivation)
3. [Tree](#tree)
4. [Verification](#verification)
   1. [On Chain Messages](#on-chain-messages)
   2. [RepoSnap](#reposnap)

----

# RPD

1. This repo is primarily for the development of a different type of license which will have relevance in the $\color{#FF00F3} \colorbox{#004F04}{al-Ākhirah}$
2. This repo will undergo several changes and a final form may not actually exist 
3. Actual document is a markdown document will be in [`doc/`](./doc)

# Motivation

> Getting tangile and lasting compensation for work that is being is done for $\color{#FF00F3} \colorbox{#004F04}{fisabilillah}$

# Tree 

The actual [license](./doc/sw.md) is being drafted in the [`doc/`](./doc/) folder.

# Verification 

<details>

<summary> <i>Details<i> </summary>

> The following are types of verification for the authenticity of the content of this repo. Open an issue for queries

## On Chain Messages 

> The following message has been encoded into the `tx`. You should be able to see the message in the `tx hash`.

```ml 
https://gitlab.com/publikdoks/sadaqaware-license
SadaqaWare License Verfication

https://quran.com/112
Surah Al Iklhas - Sincerity 
Say, He is Allāh, -who-is- One,
Allāh, the Eternal Refuge,
He neither begets nor is born,
Nor is there to Him any equivalent.

```

>TX Hashes on seperate chain 

Chain | Hash
|:--:|:--:|
[`Sepolia`](https://sepolia.etherscan.io/tx/0x163a06e737c45cac37e884216cb6345efc508fdd1b83e1ed63edc5f2f7a8b907) | Sepolia Test Chain 
[`Matic`](https://mumbai.polygonscan.com/tx/0x6ade4657b591c119ad6dcbfcf650e3484593464184352caa337ab7eb0c16674e) | Matic Mumbai 
[`BscTestNet`](https://testnet.bscscan.com/tx/0xd76d9bc9147c9c70d65d944a29f4ea9eb88fed3604ae711b8ad3c8b9664a350d) | BSC Test Net 

## RepoSnap 

I | Des
|:--:|:--:|
![](./s/output.png) | Code snapshot image 
[`Hash`](./s/output_512Hash.txt) | SHA512 of above image

</details>