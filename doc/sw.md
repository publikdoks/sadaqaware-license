<img src="../s/bi.png" align="center" width="200">

----
1. [🕋 SadaqaWare License agreement](#-sadaqaware-license-agreement)
2. [👁️ Definitions](#️-definitions)
3. [🫵 Conditions](#-conditions)
   1. [🕌 For Muslims](#-for-muslims)
   2. [🌱 For NonMuslims](#-for-nonmuslims)
4. [⚠️ General Advice](#️-general-advice)
----

# 🕋 SadaqaWare License agreement 

*This license is designed to be read in DARKMODE ONLY*

> This is an evolving license which has relevance only in the $\color{B3FFAE} \colorbox{#004F04}{al-Ākhirah}$, while agreeing to it in $\color{FF78F0} \colorbox{4C0027}{ad-Dunya}$

# 👁️ Definitions 

Acronym |Term | Definition 
|:--:|:--:|:--:|
$\color{00F5FF} \colorbox{03001C} {1P}$ | First Party | _Who invokes the usage of this license ie., the original author of this license and subsequent invokers._
$\color{FDFF00} \colorbox{03001C} {2P}$ | Second Party | _Who is bound by the terms of this license._
$\color{B3FFAE} \colorbox{#004F04}{A-A}$| $\color{B3FFAE} \colorbox{#004F04}{al-Ākhirah}$ | _Refers to the after life in ISLAM_
$\color{FF78F0} \colorbox{4C0027}{A-D}$ | $\color{FF78F0} \colorbox{4C0027}{ad-Dunya}$ | _Refers to this world, the earth_


# 🫵 Conditions 

> This license has no legal binding or relevance in $\color{FF78F0} \colorbox{4C0027}{A-D}$

## 🕌 For Muslims 

1. $\color{FDFF00} \colorbox{03001C} {2P}$ is able to use for both commerical and non commercial purposes with no attribution.
2. $\color{FDFF00} \colorbox{03001C} {2P}$ is allowed to modify it in any form.
3. $\color{FDFF00} \colorbox{03001C} {2P}$ is wholly responsible for anything deteriment arising from the usage, claims of any kind are nonexistent in $\color{FF78F0} \colorbox{4C0027}{A-D}$.
4. $\color{FDFF00} \colorbox{03001C} {2P}$ is obligated to make a charity which can be catgorized as [`Sadaqah-Jaariya`](https://muslimhands.org.uk/latest/2018/09/what-is-sadaqah-jariyah) in the name of $\color{00F5FF} \colorbox{03001C} {1P}$ (referring to $\color{00F5FF} \colorbox{03001C} {1P}$ as **Author** ) or make **dua** for $\color{00F5FF} \colorbox{03001C} {1P}$ in their **tahajjud prayers** , if none of these is done, then it will be considered that you have a debt to pay to $\color{00F5FF} \colorbox{03001C} {1P}$ *who will then forgive it in front of the Most-Meriful on the Day Of Standing*.

## 🌱 For NonMuslims 

1. $\color{FDFF00} \colorbox{03001C} {2P}$ is to make anonymous donations in the name of $\color{00F5FF} \colorbox{03001C} {1P}$ (referring to $\color{00F5FF} \colorbox{03001C} {1P}$ as **Author** ) weekly for 3 months. If this condition is not fulfilled. Then :
   1. $\color{FDFF00} \colorbox{03001C} {2P}$ must study ISLAM for 6 months on a weekly basis with sincerity.
      1. If $\color{FDFF00} \colorbox{03001C} {2P}$ is unwilling to do even this, then it would be a condsidered that a debt has been establish between $\color{00F5FF} \colorbox{03001C} {1P}$ & $\color{FDFF00} \colorbox{03001C} {2P}$ in which $\color{FDFF00} \colorbox{03001C} {2P}$ owes a debt to $\color{00F5FF} \colorbox{03001C} {1P}$ and $\color{00F5FF} \colorbox{03001C} {1P}$ will reclaim this debt in $\color{B3FFAE} \colorbox{#004F04}{A-A}$

# ⚠️ General Advice

> You are advised to not proceed without fully understanding what the terms are referring to in this license. The reprecussions are only for $\color{B3FFAE} \colorbox{#004F04}{A-A}$ and they have no relevance in $\color{FF78F0} \colorbox{4C0027}{A-D}$

